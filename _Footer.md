**Credits:**

**Hereva universe:** created by David Revoy, with contributions by Craig Maloney.  
**Corrections/Small-contributions:** Willem Sonke, Moini, Hali, Cgand, scribblemaniac and Alex Gryson.   
**Artwork:** David Revoy.  


![CC-By license image](https://www.peppercarrot.com/data/wiki/medias/img/logo_cc.png)  
[Creative Commons Attribution, CC-By](https://creativecommons.org/licenses/by/4.0/)  
Want to reuse something here? [Check the best practise](https://wiki.creativecommons.org/wiki/best_practices_for_attribution). 
