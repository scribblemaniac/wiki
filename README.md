![Header cover image of Pepper&Carrot wiki](https://www.peppercarrot.com/data/wiki/medias/img/Peppercarrot-header_wiki.jpg)

This repository contains the wiki of [Pepper&Carrot](https://www.peppercarrot.com/) with its markdown files about **Hereva**, the open-world of Pepper&Carrot. This repository exists to help the community project and [derivations](https://www.peppercarrot.com/categorie4/derivations) to extend or reuse better the universe of the webcomic.

## Veracity of this material

Much like the ever-changing world of Hereva this Wiki is also ever changing. Certain ideas may appear and then disappear without warning. This Wiki exists to build out and describe the world of Hereva, the characters, and denizens within. Only the comic should be regarded as "canon" (eg: characters that have appeared, events, animals, quotes, etc.).

That said, this Wiki also exists to help others build with and build upon the world of Hereva. We're all working on this together.

## License

Authors of all editions or contributions to this project accept to release their work under [CC-By 4.0](https://creativecommons.org/licenses/by/4.0/) license.
  
All resources here (images/sounds/videos) are under the same license. Attributions can be found on the footer of the page when the wiki is online, or on the **_Footer.md** page in the repository. A copy is also added to the CONTRIBUTORS.md file at the root of the peppercarrot/webcomic and visible online under the Author category.
